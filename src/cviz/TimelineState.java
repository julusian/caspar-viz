package cviz;

public enum TimelineState {
    ERROR,
    READY,
    CUE,
    RUN,
    CLEAR
}
