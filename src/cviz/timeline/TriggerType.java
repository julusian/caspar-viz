package cviz.timeline;

public enum TriggerType {
    IMMEDIATE,
    FRAME,
    END,
    CUE
}
